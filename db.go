package encbook

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/url"
	"os"
	"time"

	_ "github.com/cretz/go-sqleet/sqlite3"
	vcard "github.com/emersion/go-vcard"
	"github.com/emersion/go-webdav/carddav"
)

type vCardDB struct {
	db *sql.DB
}

func newVCardDB(path, encryptionKey string) (*vCardDB, error) {
	newDB := false
	if _, err := os.Stat(path); os.IsNotExist(err) {
		newDB = true
	}

	db, err := sql.Open("sqleet", path+"?_key="+url.QueryEscape(encryptionKey))
	if err != nil {
		return nil, err
	}

	v := &vCardDB{db: db}
	if newDB {
		if err := v.init(); err != nil {
			db.Close()
			return nil, err
		}
	}

	return v, nil
}

func (v *vCardDB) init() error {
	_, err := v.db.Exec(
		`CREATE TABLE vcards (
			path TEXT PRIMARY KEY,
			version INTEGER,
			vcard TEXT,
			mtime DATETIME
		 )`)
	return err
}

func (v *vCardDB) Close() {
	v.db.Close()
}

func (v *vCardDB) withTransaction(f func(*sql.Tx) error) error {
	tx, err := v.db.Begin()
	if err != nil {
		return err
	}
	if err := f(tx); err != nil {
		tx.Rollback() // nolint
		return err
	}
	return tx.Commit()
}

func (v *vCardDB) PutAddressObject(path string, card vcard.Card) (string, error) {
	log.Printf("Put(%s, %+v)", path, card)

	data, err := encodeVCard(card)
	if err != nil {
		return "", err
	}

	uidField := card.Get(vcard.FieldUID)
	if uidField == nil {
		return "", errors.New("card has no UID")
	}

	return path, v.withTransaction(func(tx *sql.Tx) error {
		var version int
		tx.QueryRow(
			`SELECT version FROM vcards WHERE path = ?`,
			path).Scan(&version)

		// These two queries are built so that the order of
		// arguments stays the same.
		stmt := `INSERT INTO vcards (version, vcard, mtime, path) VALUES (?, ?, ?, ?)`
		if version > 0 {
			stmt = `UPDATE vcards SET version=?, vcard=?, mtime=? WHERE path=?`
		}

		_, err := tx.Exec(stmt, version+1, data, time.Now(), path)
		return err
	})
}

func (v *vCardDB) GetAddressObject(path string, req *carddav.AddressDataRequest) (*carddav.AddressObject, error) {
	log.Printf("Get(%s, %+v)", path, *req)

	var out *carddav.AddressObject
	err := v.withTransaction(func(tx *sql.Tx) error {
		row := tx.QueryRow(
			`SELECT path, version, vcard, mtime FROM vcards
                         WHERE path = ?`,
			path,
		)
		var err error
		out, err = rowToAddressObject(row, req)
		return err
	})
	if err != nil {
		log.Printf("Get() ERROR: %v", err)
	}
	return out, err
}

func (v *vCardDB) ListAddressObjects(req *carddav.AddressDataRequest) ([]carddav.AddressObject, error) {
	log.Printf("List(%+v)", *req)

	var out []carddav.AddressObject
	err := v.withTransaction(func(tx *sql.Tx) error {
		rows, err := tx.Query(
			`SELECT path, version, vcard, mtime FROM vcards`,
		)
		if err != nil {
			return err
		}
		defer rows.Close()

		for rows.Next() {
			o, err := rowToAddressObject(rows, req)
			if err != nil {
				return err
			}
			out = append(out, *o)
		}
		return nil
	})
	if err != nil {
		log.Printf("List() ERROR: %v", err)
	}
	return out, err
}

type rowScanner interface {
	Scan(...interface{}) error
}

func rowToAddressObject(row rowScanner, req *carddav.AddressDataRequest) (*carddav.AddressObject, error) {
	var path string
	var version int
	var data []byte
	var mtime time.Time

	if err := row.Scan(&path, &version, &data, &mtime); err != nil {
		return nil, err
	}

	card, err := vcard.NewDecoder(bytes.NewReader(data)).Decode()
	if err != nil {
		return nil, err
	}

	// TODO: create a new Card with just the values requested in req.

	return &carddav.AddressObject{
		Path:    path,
		ModTime: mtime,
		ETag:    computeETag(path, version),
		Card:    card,
	}, nil
}

func computeETag(path string, version int) string {
	return fmt.Sprintf("%s.%d", path, version)
}

func encodeVCard(card vcard.Card) ([]byte, error) {
	var buf bytes.Buffer
	if err := vcard.NewEncoder(&buf).Encode(card); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
