package encbook

import (
	"log"
	"net/http"

	"github.com/emersion/go-webdav/carddav"
)

func Handler(dbMgr dbManager) http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/.well-known/carddav", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/", http.StatusFound)
	})
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Decipher HTTP Basic Auth.
		username, password, ok := r.BasicAuth()
		if !ok {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		// TODO: There is no authentication, we're just using
		// the password as the key.  This is only good for
		// testing purposes.
		be, err := newUserAddressBook(dbMgr, username, password)
		if err != nil {
			log.Printf("encbook.New() error: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer be.Release()

		h := carddav.Handler{Backend: be}
		h.ServeHTTP(w, r)
	})

	return mux
}
