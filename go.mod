module git.autistici.org/ai3/tools/encbook

require (
	github.com/cretz/go-sqleet v0.0.0-20180806225354-3aa61213a6ce
	github.com/emersion/go-vcard v0.0.0-20200508080525-dd3110a24ec2
	github.com/emersion/go-webdav v0.3.0
	golang.org/x/sync v0.0.0-20200930132711-30421366ff76
)
