package main

import (
	"flag"
	"net/http"

	"git.autistici.org/ai3/tools/encbook"
)

var (
	root = flag.String("root", ".", "root storage dir")
	addr = flag.String("addr", ":4365", "address to listen on")
)

func main() {
	flag.Parse()

	dbMgr := encbook.NewVCardDBManager(*root)

	http.ListenAndServe(*addr, encbook.Handler(dbMgr))
}
