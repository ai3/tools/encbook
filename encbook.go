package encbook

import (
	"errors"
	"fmt"

	vcard "github.com/emersion/go-vcard"
	"github.com/emersion/go-webdav/carddav"
)

var (
	maxResourceSize = 1024 * 1024
)

type database interface {
	GetAddressObject(string, *carddav.AddressDataRequest) (*carddav.AddressObject, error)
	PutAddressObject(string, vcard.Card) (string, error)
	ListAddressObjects(*carddav.AddressDataRequest) ([]carddav.AddressObject, error)
	Release()
}

// type Backend interface {
//     AddressBook() (*AddressBook, error)
//     GetAddressObject(path string, req *AddressDataRequest) (*AddressObject, error)
//     ListAddressObjects(req *AddressDataRequest) ([]AddressObject, error)
//     QueryAddressObjects(query *AddressBookQuery) ([]AddressObject, error)
//     PutAddressObject(path string, card vcard.Card) (loc string, err error)
//     DeleteAddressObject(path string) error
// }

// Turns a 'database' into a carddav.Backend.
type userAddressBook struct {
	database

	username string
}

type dbManager interface {
	GetDatabaseForUser(string, string) (database, error)
}

func newUserAddressBook(dbMgr dbManager, username, encryptionKey string) (*userAddressBook, error) {
	db, err := dbMgr.GetDatabaseForUser(username, encryptionKey)
	if err != nil {
		return nil, err
	}

	return &userAddressBook{
		database: db,
		username: username,
	}, nil
}

func (b *userAddressBook) AddressBook() (*carddav.AddressBook, error) {
	return &carddav.AddressBook{
		Path:            "/",
		Name:            "Contacts",
		Description:     fmt.Sprintf("Contacts for %s", b.username),
		MaxResourceSize: int64(maxResourceSize),
		SupportedAddressData: []carddav.AddressDataType{
			{ContentType: vcard.MIMEType, Version: "3"},
			{ContentType: vcard.MIMEType, Version: "4"},
		},
	}, nil
}

func (b *userAddressBook) QueryAddressObjects(req *carddav.AddressBookQuery) ([]carddav.AddressObject, error) {
	return nil, errors.New("not implemented")
}

func (b *userAddressBook) DeleteAddressObject(path string) error {
	return errors.New("not implemented")
}
