encbook
===

CardDAV server with per-user encryption of the data (prototype).

Data is stored in a separate encrypted SQLite database (using
[sqleet](https://github.com/resilar/sqleet)) for each user, so that we
can encrypt it with a user-specific key. The user-specific key should
be a long-term key that can be itself decrypted using the user
password; eventually a
[service](https://git.autistici.org/id/keystore) could be used to
provide it. **Since this is just a prototype, there is no authentication
and the encryption key is just the user's password.**

## Usage

To build the server (it's using Go modules):

```
$ go build cmd/encbook
```

Run the resulting *encbook* binary telling it an address to listen to
and a directory to use for the database files:

```
$ mkdir data
$ ./encbook --addr=localhost:3000 --root=data
```
