package encbook

import (
	"log"
	"path/filepath"
	"sync"
	"time"

	"golang.org/x/sync/singleflight"
)

var entryTTL = 10 * time.Minute

type cacheEntry struct {
	*vCardDB

	mx       sync.Mutex
	expire   time.Time
	refCount int
}

func (c *cacheEntry) Acquire() {
	c.mx.Lock()
	c.refCount++
	c.mx.Unlock()
}

func (c *cacheEntry) Release() {
	c.mx.Lock()
	c.refCount--
	if c.refCount == 0 {
		c.expire = time.Now().Add(entryTTL)
	}
	c.mx.Unlock()
}

// VCardDBManager manages access to per-user databases, caching open
// connections. The singleflight.Group + sync.Map combination lets us
// block (for database open and eventual initial creation) only that
// specific user, and we employ reference-counting on the cached
// entries to avoid closing a database that is still in use.
type VCardDBManager struct {
	root string

	sf  singleflight.Group
	dbs sync.Map
}

func NewVCardDBManager(path string) *VCardDBManager {
	v := &VCardDBManager{
		root: path,
	}
	go func() {
		t := time.NewTicker(1 * time.Minute)
		for range t.C {
			v.purge()
		}
	}()
	return v
}

func (m *VCardDBManager) GetDatabaseForUser(username, encryptionKey string) (database, error) {
	path := filepath.Join(m.root, username+".db")

	x, err, _ := m.sf.Do(username, func() (interface{}, error) {
		entry, ok := m.dbs.Load(username)
		if !ok {
			log.Printf("opening db %s for user %s", path, username)
			db, err := newVCardDB(path, encryptionKey)
			if err != nil {
				return nil, err
			}
			entry = &cacheEntry{vCardDB: db}
			m.dbs.Store(username, entry)
		}

		return entry, nil
	})
	if err != nil {
		return nil, err
	}

	entry := x.(*cacheEntry)
	entry.Acquire()

	return entry, err
}

func (m *VCardDBManager) purge() {
	m.dbs.Range(func(key, value interface{}) bool {
		entry := value.(*cacheEntry)
		entry.mx.Lock()
		defer entry.mx.Unlock()
		if entry.refCount == 0 && entry.expire.Before(time.Now()) {
			m.dbs.Delete(key)

			entry.Close()
		}
		return true
	})
}
